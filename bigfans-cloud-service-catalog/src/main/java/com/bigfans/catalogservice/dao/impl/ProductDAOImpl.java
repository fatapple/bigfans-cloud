package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.ProductDAO;
import com.bigfans.catalogservice.model.Product;
import com.bigfans.framework.dao.BeanDecorator;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.framework.model.PageBean;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(ProductDAOImpl.BEAN_NAME)
public class ProductDAOImpl extends MybatisDAOImpl<Product> implements ProductDAO {

	public static final String BEAN_NAME = "productDAO";

	@Override
	public List<Product> listByCategory(String catId, Long start , Long pagesize) {
		ParameterMap params = new ParameterMap(start, pagesize);
		params.put("categoryId", catId);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public PageBean<Product> search(Product params, Long start, Long pagesize) {
		return null;
	}

	@Override
	public List<Product> listByCategory(String[] catIds, Long start , Long pagesize) {
		ParameterMap params = new ParameterMap(start, pagesize);
		params.put("categoryIds", catIds);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public void updateHits(String productId) {
		getSqlSession().update(className + ".updateHits", productId);
	}
	
	@Override
	public int updateThumb(String prodId, String thumbPath) {
		Product updateCondition = new Product();
		updateCondition.setId(prodId);
		updateCondition.setImagePath(thumbPath);
		return new BeanDecorator(updateCondition).update();
	}
	
	public void viewed(String productId , String userId){
		ParameterMap params = new ParameterMap();
		params.put("pid", productId);
		params.put("userId", userId);
		getSqlSession().insert(className + ".viewed", params);
	}
	
	@Override
	public List<Product> listByPgId(String pgId) {
		ParameterMap params = new ParameterMap();
		params.put("pgId", pgId);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public List<Product> listHotSale(Long start, Long pagesize) {
		ParameterMap params = new ParameterMap(start, pagesize);
		params.put("onSale", true);
		params.put("hotSale", true);
		return getSqlSession().selectList(className + ".list", params);
	}
}
