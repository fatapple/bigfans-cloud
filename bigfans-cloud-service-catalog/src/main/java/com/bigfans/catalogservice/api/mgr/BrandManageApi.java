package com.bigfans.catalogservice.api.mgr;

import com.bigfans.catalogservice.Constants;
import com.bigfans.catalogservice.model.Brand;
import com.bigfans.catalogservice.service.brand.BrandService;
import com.bigfans.framework.plugins.UploadResult;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BrandManageApi {

    @Autowired
    private BrandService brandService;

    @GetMapping(value = "/brands")
    public RestResponse list() throws Exception {
//		brandService.searchservice(keyword, start, pagesize);
        return RestResponse.ok();
    }

    @PostMapping(value = "/brand")
    public RestResponse create(@RequestBody Brand brand) throws Exception {
        brandService.create(brand);
        return RestResponse.ok();
    }

    @PostMapping(value = "/brand/uploadLogo")
    public RestResponse uploadLogo(@RequestParam("logo") MultipartFile logoFile) throws Exception {
        UploadResult uploadResult = brandService.uploadLogo(logoFile.getInputStream(), logoFile.getOriginalFilename());
        Map<String , Object> data = new HashMap<>();
        data.put("storageType", uploadResult.getStorageType());
        data.put("filePath", uploadResult.getFilePath());
        data.put("fileKey", uploadResult.getFileKey());
        return RestResponse.ok(data);
    }

    @PostMapping(value = "/brand/deleteLogo")
    public RestResponse deleteLogo(@RequestBody Map<String, Object> params) throws Exception {
        UploadResult uploadResult = brandService.deleteLogo((String) params.get("logoKey"));
        if (!uploadResult.isSuccess()) {
            return RestResponse.error(Constants.ERROR_CODE.IMG_NOT_FOUND , "删除logo失败");
        }
        return RestResponse.ok();
    }
}
