package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.AttributeOptionEntity;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @Title:
 * @Description: 商品属性选项
 * @author lichong
 * @date 2015年9月15日 下午11:30:05
 * @version V1.0
 */
@Data
public class AttributeOption extends AttributeOptionEntity {

	private static final long serialVersionUID = 1387087745406482602L;

	private String catName;
	private String requiredLabel;
	private String searchableLabel;
	private String prodId;
	private String pgId;
	private String inputTypeLabel;

	private List<AttributeValue> valueList;
	private List<String> values;

	public List<String> getValues(){
		if(valueList == null){
			return null;
		}
		return valueList.stream().map(AttributeValue::getValue).collect(Collectors.toList());
	}

	public String getInputTypeLabel() {
		if (inputType.equals("M")) {
			inputTypeLabel = "手动输入";
		} else if (inputType.equals("L")) {
			inputTypeLabel = "从列表选择";
		}
		return inputTypeLabel;
	}

	public String getRequiredLabel(){
		return this.required ? "是" : "否";
	}

	public String getSearchableLabel(){
		return this.searchable ? "是" : "否";
	}
}
