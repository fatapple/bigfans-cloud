package com.bigfans.catalogservice.service.spec;

import com.bigfans.catalogservice.dao.ProductSpecDAO;
import com.bigfans.catalogservice.model.ProductSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-24 上午9:39
 **/
@Service(SpecServiceImpl.BEAN_NAME)
public class SpecServiceImpl implements SpecService {

    public static final String BEAN_NAME = "specService";

    @Autowired
    private ProductSpecDAO productSpecDAO;

    public ProductSpec getProductSpec(String pgId, String optionId, String valueId) throws Exception{
        return productSpecDAO.getProductSpec(pgId , optionId , valueId);
    }

    @Override
    public List<ProductSpec> listProductSpecs(String prodId) throws Exception {
        return productSpecDAO.listByProdId(prodId);
    }
}
