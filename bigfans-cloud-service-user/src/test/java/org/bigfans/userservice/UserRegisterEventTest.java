package org.bigfans.userservice;

import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.user.UserRegisteredEvent;
import com.bigfans.userservice.UserServiceApp;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author lichong
 * @create 2018-04-14 下午12:02
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=UserServiceApp.class)
public class UserRegisterEventTest extends TestCase
{
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Test
    public void testPublish(){
        kafkaTemplate.send(new UserRegisteredEvent("1"));
        kafkaTemplate.send(new UserRegisteredEvent("2"));
        kafkaTemplate.send(new UserRegisteredEvent("3"));
        kafkaTemplate.send(new UserRegisteredEvent("4"));
        kafkaTemplate.send(new UserRegisteredEvent("5"));
        kafkaTemplate.send(new UserRegisteredEvent("6"));
        kafkaTemplate.send(new UserRegisteredEvent("7"));
        kafkaTemplate.send(new UserRegisteredEvent("8"));
        kafkaTemplate.send(new UserRegisteredEvent("9"));
        kafkaTemplate.send(new UserRegisteredEvent("23"));
    }

}
