package com.bigfans.userservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.userservice.dao.FavoritesDAO;
import com.bigfans.userservice.model.Favorites;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 10, 2016 3:27:16 PM 
 *
 */
@Repository(FavoritesDAOImpl.BEAN_NAME)
public class FavoritesDAOImpl extends MybatisDAOImpl<Favorites> implements FavoritesDAO {

	public static final String BEAN_NAME = "favoritesDAO";
	
	@Override
	public List<Favorites> listByUser(String userId, Long start, Long pagesize) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("start", start);
		params.put("pagesize", pagesize);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public Favorites getByUser(String userId, String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("prodId", prodId);
		return getSqlSession().selectOne(className + ".load", params);
	}
}
