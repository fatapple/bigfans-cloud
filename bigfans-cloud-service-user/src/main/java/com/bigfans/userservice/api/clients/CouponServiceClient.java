package com.bigfans.userservice.api.clients;

import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.userservice.UserApplications;
import com.bigfans.userservice.model.Coupon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
public class CouponServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Coupon> getCoupon(String couponId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate , UserApplications.getFunctionalUser());
            Map data = serviceRequest.get(Map.class , "http://pricing-service/coupons/{id}" , couponId);
            Coupon coupon = BeanUtils.mapToModel(data , Coupon.class);
            return coupon;
        });
    }
}
