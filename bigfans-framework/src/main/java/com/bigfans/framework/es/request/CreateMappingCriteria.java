package com.bigfans.framework.es.request;

import java.io.Serializable;

import com.bigfans.framework.es.Mapping;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年7月27日上午9:56:45
 *
 */
public class CreateMappingCriteria implements Serializable{

	private static final long serialVersionUID = -8756373872969653255L;
	private Long version;
	private String indexName;
	private String type;
	private Mapping mappingBuilder;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Mapping getMappingBuilder() {
		return mappingBuilder;
	}
	
	public void setMappingBuilder(Mapping mappingBuilder) {
		this.mappingBuilder = mappingBuilder;
	}

}
