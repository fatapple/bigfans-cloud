package com.bigfans.framework.dao;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @Description: ID 生成策略,默认为自增
 * @author lichong
 * @date Nov 11, 2016 10:20:27 AM 
 *
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface GeneratedID {

	Class<? extends IDGenerator> generator() default AutoIncrementIDGenerator.class;
	
}
