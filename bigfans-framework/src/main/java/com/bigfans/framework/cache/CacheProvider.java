package com.bigfans.framework.cache;

import java.util.List;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 2, 2016 8:29:57 PM 
 *
 */
public interface CacheProvider {

	public CacheEntry get(CacheKey key);
	
	public void put(CacheEntry value);
	
	public void update(CacheEntry value);

	public void evict(CacheKey key);
	
	public void evict(List<CacheKey> keys);
	
	public void clear();
	
}
