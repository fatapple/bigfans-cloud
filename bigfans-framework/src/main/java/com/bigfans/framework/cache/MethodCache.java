package com.bigfans.framework.cache;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 
 * @Title: 缓存方法返回的结果
 * @Description: 
 * @author lichong 
 * @date 2016年1月24日 下午5:04:21 
 * @version V1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface MethodCache {

	/**
	 * 缓存key生成器
	 * @return
	 */
	Class<? extends CacheKeyGenerator> keyGenerator() default CacheKeyGenerator.class;
	
	/**
	 * 过期时间
	 * @return
	 */
	int expireIn() default 0;
	
	/**
	 * 设置缓存key，如果设置了这个选项就不会通过@CacheKeyGenerator 来生成key值。
	 * @return
	 */
	String key() default "";
}
