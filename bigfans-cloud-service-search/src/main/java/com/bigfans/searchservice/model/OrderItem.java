package com.bigfans.searchservice.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItem {

    protected String orderId;
    protected String userId;
    /* 产品id */
    protected String prodId;
    /* 产品成交价 */
    protected BigDecimal dealPrice;
    /* 产品成交总价 */
    protected BigDecimal dealSubTotal;
    /* 购买数量 */
    protected Integer quantity;
    //获得积分
    protected Float gainedPoint;
    // 评价状态
    protected Integer commentStatus;
    protected String promotion;
}
